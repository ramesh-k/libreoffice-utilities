REM  *****  BASIC  *****

Sub Create_VCF()
    Dim FileNum     As Integer
    Dim RowNo        As Double
    Dim DateTimeStamp As String
    RowNo = 1
    FileNum = FreeFile
    ContactList = ThisComponent.Sheets.getByName("Sheet1")
    DateTimeStamp = Format(Now(),"YYYYMMDD-HHMMSS")
    GlobalScope.BasicLibraries.loadLibrary("Tools")
    OutFilePath=DirectoryNameoutofPath(ThisComponent.getURL(),"/") & "/Contact-VCF-" & DateTimeStamp & ".VCF"
    Open OutFilePath For Output As FileNum
    While Trim(ContactList.getCellByPosition(1,RowNo).String) <> ""
        FName = Trim(ContactList.getCellByPosition(1,RowNo).String)
        LName = Trim(ContactList.getCellByPosition(2,RowNo).String)
        Phone1 = Trim(ContactList.getCellByPosition(3,RowNo).String)
        Phone2 = Trim(ContactList.getCellByPosition(4,RowNo).String)
        EmailID = Trim(ContactList.getCellByPosition(5,RowNo).String)
        Print #FileNum, "BEGIN:VCARD"
        Print #FileNum, "VERSION:3.0"
        Print #FileNum, "FN:" & FName & " " & LName
        Print #FileNum, "N:" & LName & ";" & FName & ";;;"
        Print #FileNum, "EMAIL;TYPE=INTERNET;TYPE=PREF:" & EmailID
        Print #FileNum, "TEL;TYPE=CELL;TYPE=PREF:" & Phone1
        Print #FileNum, "TEL;TYPE=VOICE:" & Phone2
        Print #FileNum, "END:VCARD"
        RowNo = RowNo + 1
    Wend
    Close #FileNum
    MsgBox "Contacts Converted to VCF 3.0 and Saved To:"& chr(10) & OutFilePath
End Sub

'Last Updated on 31-05-2021 by Ramesh Kunnappully
