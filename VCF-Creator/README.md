# VCF Creator
This utility (spreadsheet) helps to create a **VCF 3.0** file using [LibreOffice Calc](https://www.libreoffice.org/discover/calc/). 

### Steps
1. Download [**this zip file**](https://gitlab.com/ramesh-k/libreoffice-utilities/-/archive/main/libreoffice-utilities-main.zip?path=VCF-Creator/VCF-Creator.ods) and extract **VCF-Creator.ods** spreadsheet file.
2. After enabling macros in LibreOffice Calc open the VCF-Creator spreadsheet file with LibreOffice Calc.
3. Enter necessary data in Sheet1 of VCF-Creator spreadsheet.
4. Click on **Create VCF** button to create VCF File.

### LibreOffice Basic Macro
[VCF-Creator.bas](./VCF-Creator.bas) Macro used in this spreadsheet.
