﻿REM  *****  BASIC  *****

Public setSheet     As Object
Public ManualStop   As Boolean
Public ManualStopAct As Boolean

Sub Main()
    
Dim api_url         As String
Dim bot_token       As String
Dim chat_id         As String
Dim is_anonymous    As Boolean
Dim qtype           As String
Dim protect_content As Boolean
Dim period          As Integer
Dim delay           As Integer
Dim queSheet        As Object
Dim qstart          As Long
Dim qend            As Long
Dim startmsg        As String
Dim endmsg          As String
Dim start_delay     As Integer
Dim x               As Integer
Dim y               As Integer
Dim mode            As Integer
Dim question        As String
Dim options         As String
Dim option1         As String
Dim option2         As String
Dim option3         As String
Dim option4         As String
Dim correct_option_id As Integer
Dim explanation     As String
Dim objRequest      As Object
Dim strPostData     As String
Dim spoiler			As Boolean
    
setSheet = ThisComponent.Sheets.getByName("Main")
'Select worksheet, which contain Question and Answers
queSheet = ThisComponent.Sheets.getByName(setSheet.getCellRangeByName("b15").String)

'Telegram Bot API URL
api_url = "https://api.telegram.org/bot"

'Bot Token
bot_token = setSheet.getCellRangeByName("b6").String

'Chat ID of Person/Group/Channel
chat_id = setSheet.getCellRangeByName("b10").String

'Is quiz Anonymous
is_anonymous = setSheet.getCellRangeByName("b11").String

'Poll should be Quiz mode
qtype = "quiz"

'Protect Message and Quiz
protect_content = setSheet.getCellRangeByName("b12").String

'Quiz time out
period = setSheet.getCellRangeByName("b13").value

'Delay for Quiz posting in Seconds
delay = setSheet.getCellRangeByName("b14").value * 1000

'Start Question No.
qstart = setSheet.getCellRangeByName("b16").Value

'End Question No.
qend = setSheet.getCellRangeByName("b17").value

'Message at Start
startmsg = setSheet.getCellRangeByName("b18").String

'Message at End
endmsg = setSheet.getCellRangeByName("b19").String

'Delay after Start Message
start_delay = setSheet.getCellRangeByName("b20").value * 1000

'Use spoiler text, if Q & A mode
spoiler = setSheet.getCellRangeByName("b22").String

ManualStopAct = TRUE

'Start Message

setSheet.getCellRangeByName("b23").string = "Starting.."

If startmsg = "" Then
    
Else
    
    strPostData = "protect_content=" & protect_content & "&chat_id=" & chat_id & "&text=" & startmsg
    objRequest  = createUnoService("com.sun.star.bridge.OleObjectFactory").createInstance("MSXML2.XMLHTTP.6.0")
    With objRequest
        .open "POST", api_url & bot_token & "/sendMessage?", FALSE
        .setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
        .send(strPostData)
        'msgbox .responseText
    End With
    
    Wait start_delay
    
End If

'Set alternate value for Start Question no.
If qstart = "" Then
    qstart = 1
Else
    If qstart = 0 Then
        qstart = 1
    End If
End If

'Set alternate value for End Question no.
If qend = "" Then
    qend = 9999999
Else
    If qend = 0 Then
        qend = 9999999
    End If
End If

'Value of y
y = qstart

'Loop Start
Do Until queSheet.getCellByPosition(1,y).string = "" Or y > qend Or ManualStop = TRUE

	'Select mode
	IF setSheet.getCellRangeByName("b21").String = "QUESTION AND ANSWER" Then
	mode = "1"
	Else
    	If  queSheet.getCellByPosition(3,y).string = "" Then
       		mode = "1"
    	Else
        	mode = "0"
    	End If
	End If
    
    'Question with Serial No.
    question = y & ". " & queSheet.getCellByPosition(1,y).String
    
    'Explanation of Question / answer
    explanation = queSheet.getCellByPosition(7,y).String
    
    'Set options available
    option1 = """" & queSheet.getCellByPosition(2,y).String & ""","
    
    If mode = "1" Then
    	x = 2
    		if queSheet.getCellByPosition(6,y).value + 1 > x then
   		 		x = queSheet.getCellByPosition(6,y).value + 1
    		End if
    
    	'Post Question with answer    
    	if spoiler = TRUE then    
    	strPostData = "chat_id=" & chat_id & "&parse_mode=html" & "&protect_content=" & protect_content & "&text=" & question & "%0A%0A%E2%9C%85%20<tg-spoiler>" & queSheet.getCellByPosition(x,y).string & "%0A%0A" & explanation & "</tg-spoiler>"
   		Else
    	strPostData = "chat_id=" & chat_id & "&parse_mode=html" & "&protect_content=" & protect_content & "&text=" & question & "%0A%0A%E2%9C%85%20" & queSheet.getCellByPosition(x,y).string & "%0A%0A" & explanation
   	 	End if        
        objRequest  = createUnoService("com.sun.star.bridge.OleObjectFactory").createInstance("MSXML2.XMLHTTP.6.0")
        With objRequest
            .Open "POST", api_url & bot_token & "/sendMessage?", FALSE
            .setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
            .send(strPostData)
            'msgbox .responseText
        End With
        setSheet.getCellRangeByName("b23").string = "Question No. " & y & " posted"
    Else
        
        If queSheet.getCellByPosition(4,y).String = "" Then
            
            option2 = """" & queSheet.getCellByPosition(3,y).String & """"
            option3 = ""
            option4 = ""
            
        Else
            
            option2 = """" & queSheet.getCellByPosition(3,y).String & ""","
            option3 = """" & queSheet.getCellByPosition(4,y).String & ""","
            
            If queSheet.getCellByPosition(5,y).String = "" Then
                
                option3 = """" & queSheet.getCellByPosition(4,y).String & """"
                option4 = ""
                
            Else
                
                option3 = """" & queSheet.getCellByPosition(4,y).String & ""","
                option4 = """" & queSheet.getCellByPosition(5,y).String & """"
                
            End If
        End If
        
        'Options
        options = "[" & option1 & option2 & option3 & option4 & "]"
        
        'Correct Answer
        correct_option_id = queSheet.getCellByPosition(6,y).value - 1
        
        
        strPostData = "chat_id=" & chat_id & "&is_anonymous=" & is_anonymous & "&type=" & qtype & "&correct_option_id=" & correct_option_id & "&open_period=" & period & "&protect_content=" & protect_content & "&question=" & question & "&options=" & options & "&correct_option_id=" & correct_option_id & "&explanation=" & explanation
        
        'Post Question with Options
        objRequest  = createUnoService("com.sun.star.bridge.OleObjectFactory").createInstance("MSXML2.XMLHTTP.6.0")
        With objRequest
            .Open "POST", api_url & bot_token & "/sendpoll?", FALSE
            .setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
            .send(strPostData)
            'msgbox .responseText
        End With
        setSheet.getCellRangeByName("b23").string = "Question No. " & y & " posted"
        
    End If
    
    'Wait...
    wait delay
    
    y = y + 1
    
Loop

'End Message
If endmsg = "" Then
    
Else
    
    strPostData = "protect_content=" & protect_content & "&chat_id=" & chat_id & "&text=" & endmsg
    objRequest  = createUnoService("com.sun.star.bridge.OleObjectFactory").createInstance("MSXML2.XMLHTTP.6.0")
    With objRequest
        .open "POST", api_url & bot_token & "/sendMessage?", FALSE
        .setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
        .send(strPostData)
        'msgbox .responseText
    End With
    
End If

'Alert me
setSheet.getCellRangeByName("b23").string = "Finished !"
'MsgBox "Finished !"

End Sub

Sub SetManualStop()
    If ManualStopAct = TRUE Then
        ManualStop = TRUE
        setSheet.getCellRangeByName("b23").string = "Stopping.."
    End If
    
End Sub

