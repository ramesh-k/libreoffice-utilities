# Telegram-Quiz-Bot
This utility (spreadsheet) helps to send **Quiz** to [**Telegram**](https://telegram.org/) channel / group / personal chat using [**LibreOffice Calc**](https://www.libreoffice.org/discover/calc/). 

### Steps
1. Download [**this zip file**](https://gitlab.com/ramesh-k/libreoffice-utilities/-/archive/main/libreoffice-utilities-main.zip?path=Telegram-Quiz-Bot/Telegram-Quiz-Bot.ods) and extract **Telegram-Quiz-Bot.ods** spreadsheet file.
2. After enabling macros in LibreOffice Calc open the Telegram-Quiz-Bot spreadsheet file with LibreOffice Calc.
3. Read the Help Sheet to Run.

